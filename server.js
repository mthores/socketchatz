//#############################################################################################
// Imports / Requirements
var functions = require(__dirname + "/functions");
var mongoOperator = require(__dirname + "/mongoOperator");
var fs = require("fs");
var express = require("express");
var http = require("http");
var socketIO = require("socket.io");
var mongo = require("mongodb").MongoClient;
var ObjectId = require('mongodb').ObjectID;
var bodyParser = require('body-parser');

//#############################################################################################
// App configuration
var app = express();
var server = http.Server(app);
var io = socketIO(server); // wrapping socket.io and express on the http-server to run simultaniously on same port.
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

//#############################################################################################
// Data stored in RAM
var nPort = process.env.port | 3000;
var aUsers = [];
var aSockets = [];
var aMsgHistory = [];
var sUrl = "mongodb://localhost:27017/chat";
var dbDoc = {
    "_id": new ObjectId("5943cdf61923fa0e425be047")
};

//#############################################################################################
// MongoDB connecting - Server nested in mongo.connect scope. 
mongo.connect(sUrl, function (err, oDb) {
    if (err) {
        console.log(err);
        console.log("#### Stopping server due to missing database ####");
        process.exit()
    } else {
        console.log("Connected to DB");
        // mongoOperator.emptyHistory(oDb, dbDoc);
    }

    //#############################################################################################
    // Socket.io - event protocol
    io.on("connection", function (oSocket) {

        oSocket.on("join", function (client) {
            console.log(client.username + " just connected");
            // Adding user to arrays.
            oSocket.username = client.username;
            aUsers.push(client.username);
            aSockets.push(oSocket);

            io.emit("push-userlist", {
                "status": "ok",
                "data": aUsers
            });

            oSocket.broadcast.emit("user-joined", {
                "status": "ok",
                "data": oSocket.username
            });

            oSocket.emit("push-history", {
                "status": "ok",
                "history": aMsgHistory
            });
        });

        oSocket.on("new-message", function (res) {
            res.from = oSocket.username;
            if (res.receiver === "all") {
                sMessageBackup = oSocket.username + ": " + res.message;
                aMsgHistory.push(sMessageBackup);
                mongoOperator.updateHistory(oDb, dbDoc, aMsgHistory);
                io.emit("new-message", res);

            } else {
                var oTargetSocket = functions.getSocketFromName(res.receiver, aSockets);
                oSocket.emit("private-message", res);
                if (oTargetSocket) {
                    oTargetSocket.emit("private-message", res);
                }

            }
        });

        oSocket.on("disconnect", function () {
            console.log(oSocket.username + " left the chatroom");
            aUsers.splice(aUsers.indexOf(oSocket.username), 1);
            aSockets.splice(aSockets.indexOf(oSocket), 1);

            io.emit("push-userlist", {
                "status": "ok",
                "data": aUsers
            });

            oSocket.broadcast.emit("user-disconnected", {
                "status": "ok",
                "data": oSocket.username
            })
        });
    });

    //#############################################################################################
    // Routes
    app.get("/", function (req, res) {
        res.sendFile(__dirname + "/views/index.html");
    });

    app.post("/chat", function (req, res) {
        var sUsername = req.body.username;

        if (functions.nameValid(aSockets, sUsername)) {
            fs.readFile(__dirname + "/views/chat.html", "utf8", function (err, data) {
                var htmlChat = data;
                htmlChat = htmlChat.replace("{{username}}", sUsername);
                res.send(htmlChat);
            });
        } else {
            res.redirect("/invalid?username=" + sUsername);
        }
    });

    app.get("/chat", function (req, res) {
        res.redirect("/");
    });

    app.get("/invalid", function (req, res) {
        fs.readFile(__dirname + "/views/index.html", "utf8", function (err, data) {
            var htmlIndex = data;
            var sMessage = "username " + req.query.username + " is taken or invalid";
            htmlIndex = htmlIndex.replace("Please select username", sMessage);
            res.send(htmlIndex);
        })
    });

    //#############################################################################################
    // starting server - nested in the read from db's callback. 
    var cHistory = oDb.collection("chat-history");
    cHistory.findOne(dbDoc, function (err, jHistory) {
        aMsgHistory = jHistory.history;
        server.listen(nPort, function () {
            console.log("Server running on port " + nPort);
        });
    });
});

//#############################################################################################