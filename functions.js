var functions = {};

functions.nameValid = function (aSockets, sUsername) {
    for (var i = 0; i < aSockets.length; i++) {
        if (sUsername == aSockets[i].username) {
            return false;
        }
    }
    if (sUsername.match(/^[a-zA-ZæÆøØåÅ0-9_-]*$/) && sUsername !== "") {
        return true;
    }
}

functions.getSocketFromName = function (userName, socketList) {

    var oTargetSocket;

    for (var i = 0; i < socketList.length; i++) {
        if (userName === socketList[i].username) {
            oTargetSocket = socketList[i];
            return oTargetSocket;
        }
    }
    return false;
}

module.exports = functions;