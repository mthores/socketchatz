// Use the files to interact with mongodb
var mongo = require("mongodb").MongoClient;

var mongoOperator = {};

// mongoOperator.writeHistory = function (oDb, data) {
//     var cHistory = oDb.collection("chat-history");
//     // create
//     cHistory.insert({
//         "name": "history-doc",
//         "history": data
//     }, function (err, uData) {
//         console.log(uData);
//     });
// }

// mongoOperator.readHistory = function (oDb, data, callback) {
//     var cHistory = oDb.collection("chat-history");
//     // Read the date from the collection - Json is "search parameter"
//     cHistory.find(data) /*.limit(1)*/ .toArray(function (err, ajHistory) {
//         callback();
//         console.log(ajHistory)
//     });
// }

mongoOperator.updateHistory = function (oDb, doc, data) {
    var cHistory = oDb.collection("chat-history");
    // update or edit
    var jNewData = {
        "name": "history-doc",
        "history": data
    };

    cHistory.update(doc, jNewData, function (err, uData) {
        if (err) {
            console.log(err);
        } else {
            console.log("history was saved");
        }
    });
}

mongoOperator.emptyHistory = function (oDb, doc) {
    var cHistory = oDb.collection("chat-history");
    var jNewData = {
        "name": "history-doc",
        "history": []
    };
    cHistory.update(doc, jNewData, function (err, uData) {
        if (err) {
            console.log(err);
        } else {
            console.log("History was cleared");
        }
    });
}


module.exports = mongoOperator;