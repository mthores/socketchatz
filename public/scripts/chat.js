var sUsername = $("#username").text();
var client = {
    "username": sUsername,
}

//#############################################################################################
// Other functions

// Making button react to enter-button
$(document).ready(function () {
    $('#inputField').keypress(function (e) {
        if (e.keyCode == 13)
            $('#btnSend').click();
    });
});

$("#historyCheck").change(function () {
    if (this.checked) {
        $(".message-history").css({
            "display": "block"
        });
    }
    if (!this.checked) {
        $(".message-history").css({
            "display": "none"
        });
    }
});


//############################################################################################
// Socket.io 

var socket = io.connect();

socket.on("connect", function () {
    socket.emit("join", client);
});

socket.on("push-history", function (res) {
    res.history.forEach(function (message) {
        var historyMessage = "<p class='message-history'>" + message + "</p>"
        $("#txtOutput").prepend(historyMessage);
    });
    var serverMessage = "<p class='message-server'>Welcome to SocketChat - You can write private to users by using @username </p>"
    $("#txtOutput").prepend(serverMessage);
});

socket.on("push-userlist", function (res) {
    var aUserList = res.data;
    $("#onlineUsers").empty();
    aUserList.forEach(function (user) {
        $("#onlineUsers").append("<li>" + user + "</li>");

    }, this);
});

socket.on("user-joined", function (res) {
    var sUser = res.data;
    $("#txtOutput").prepend("<p class='message-server' >Server: " + sUser + " joined the chat</p>");
});

socket.on("user-disconnected", function (res) {
    var sUser = res.data;
    $("#txtOutput").prepend("<p class='message-server' >Server: " + sUser + " left the chat</p>");
});

socket.on("new-message", function (res) {
    if (res.from === client.username) {
        $("#txtOutput").prepend("<p class='message-sent' >" + res.from + ": " + res.message + "</p>");
    } else {
        $("#txtOutput").prepend("<p class='message-recieved' >" + res.from + ": " + res.message + "</p>");
    }
});

socket.on("private-message", function (res) {
    console.log(res.from);
    console.log(res.message);
    if (res.from === client.username) {
        $("#txtOutput").prepend("<p class='message-sent' >" + res.from + " (@"+res.receiver+"): " + res.message + "</p>");
    } else {
        $("#txtOutput").prepend("<p class='message-recieved' >" + res.from + " (private): " + res.message + "</p>");
    }
});

$("#btnSend").click(function () {
    sMessage = $("#inputField").val();
    $("#inputField").val("");

    jData = {
        "status": "ok",
        "message": sMessage,
        "receiver": "all"
    };

    if (sMessage[0] === "@") {
        var nSpace = sMessage.indexOf(" ");
        var sReceiver = sMessage.slice(1, nSpace);
        jData.message = sMessage.replace("@" + sReceiver, "");
        jData.receiver = sReceiver;
        console.log(sReceiver);
    }

    socket.emit("new-message", jData);
});

$("#btnBack").click(function () {
    window.location.href = "/";
});